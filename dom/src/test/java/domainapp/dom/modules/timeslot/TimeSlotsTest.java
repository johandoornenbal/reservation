package domainapp.dom.modules.timeslot;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import org.apache.isis.applib.query.Query;

import domainapp.dom.FinderInteraction;
import domainapp.dom.modules.consumer.Consumer;
import domainapp.dom.modules.resource.Resource;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by jodo on 21/06/15.
 */
public class TimeSlotsTest {

    FinderInteraction finderInteraction;

    TimeSlots timeSlots;

    @Before
    public void setup() {
        timeSlots = new TimeSlots() {

            @Override
            protected <T> T firstMatch(Query<T> query) {
                finderInteraction = new FinderInteraction(query, FinderInteraction.FinderMethod.FIRST_MATCH);
                return null;
            }

            @Override
            protected List<TimeSlot> allInstances() {
                finderInteraction = new FinderInteraction(null, FinderInteraction.FinderMethod.ALL_INSTANCES);
                return null;
            }

            @Override
            protected <T> List<T> allMatches(Query<T> query) {
                finderInteraction = new FinderInteraction(query, FinderInteraction.FinderMethod.ALL_MATCHES);
                return null;
            }
        };
    }

    public static class FindByResource extends TimeSlotsTest {

        @Test
        public void happyCase() {

            Resource resource = new Resource();
            timeSlots.findTimeSlotByResource(resource);

            assertThat(finderInteraction.getFinderMethod(), is(FinderInteraction.FinderMethod.ALL_MATCHES));
            assertThat(finderInteraction.getQueryName(), is("findByResource"));
            assertThat(finderInteraction.getArgumentsByParameterName().get("resource"), is((Object) resource));
            assertThat(finderInteraction.getArgumentsByParameterName().size(), is(1));
        }
    }

    public static class FindByConsumer extends TimeSlotsTest {

        @Test
        public void happyCase() {

            Consumer consumer = new Consumer();
            timeSlots.findTimeSlotByConsumer(consumer);

            assertThat(finderInteraction.getFinderMethod(), is(FinderInteraction.FinderMethod.ALL_MATCHES));
            assertThat(finderInteraction.getQueryName(), is("findByConsumer"));
            assertThat(finderInteraction.getArgumentsByParameterName().get("consumer"), is((Object) consumer));
            assertThat(finderInteraction.getArgumentsByParameterName().size(), is(1));
        }
    }

}
