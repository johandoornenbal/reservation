package domainapp.dom.modules.consumer;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import org.apache.isis.applib.query.Query;

import domainapp.dom.FinderInteraction;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by jodo on 21/06/15.
 */
public class ConsumersTest {

    FinderInteraction finderInteraction;

    Consumers consumers;

    @Before
    public void setup() {
        consumers = new Consumers() {

            @Override
            protected <T> T firstMatch(Query<T> query) {
                finderInteraction = new FinderInteraction(query, FinderInteraction.FinderMethod.FIRST_MATCH);
                return null;
            }

            @Override
            protected List<Consumer> allInstances() {
                finderInteraction = new FinderInteraction(null, FinderInteraction.FinderMethod.ALL_INSTANCES);
                return null;
            }

            @Override
            protected <T> List<T> allMatches(Query<T> query) {
                finderInteraction = new FinderInteraction(query, FinderInteraction.FinderMethod.ALL_MATCHES);
                return null;
            }
        };
    }

    public static class FindById extends ConsumersTest {

        @Test
        public void happyCase() {

            String id = new String();
            consumers.findById(id);

            assertThat(finderInteraction.getFinderMethod(), is(FinderInteraction.FinderMethod.ALL_MATCHES));
            assertThat(finderInteraction.getQueryName(), is("findById"));
            assertThat(finderInteraction.getArgumentsByParameterName().get("id"), is((Object) id));
            assertThat(finderInteraction.getArgumentsByParameterName().size(), is(1));
        }
    }

    public static class AutoComplete extends ConsumersTest {

        @Test
        public void happyCase() {

            String search = new String();
            consumers.autoComplete(search);

            assertThat(finderInteraction.getFinderMethod(), is(FinderInteraction.FinderMethod.ALL_MATCHES));
            assertThat(finderInteraction.getQueryName(), is("matchId"));
            assertThat(finderInteraction.getArgumentsByParameterName().get("search"), is((Object) search));
            assertThat(finderInteraction.getArgumentsByParameterName().size(), is(1));
        }
    }

}
