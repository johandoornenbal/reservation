package domainapp.dom.modules.resource;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import org.apache.isis.applib.DomainObjectContainer;
import org.apache.isis.applib.query.Query;
import org.apache.isis.core.unittestsupport.jmocking.JUnitRuleMockery2;

import domainapp.dom.FinderInteraction;

/**
 * Created by jodo on 21/06/15.
 */
public class ResourceUnitsTest {

    FinderInteraction finderInteraction;

    ResourceUnits resourceUnits;

    @Before
    public void setup() {
        resourceUnits = new ResourceUnits() {

            @Override
            protected <T> T firstMatch(Query<T> query) {
                finderInteraction = new FinderInteraction(query, FinderInteraction.FinderMethod.FIRST_MATCH);
                return null;
            }

            @Override
            protected List<ResourceUnit> allInstances() {
                finderInteraction = new FinderInteraction(null, FinderInteraction.FinderMethod.ALL_INSTANCES);
                return null;
            }

            @Override
            protected <T> List<T> allMatches(Query<T> query) {
                finderInteraction = new FinderInteraction(query, FinderInteraction.FinderMethod.ALL_MATCHES);
                return null;
            }
        };
    }

    public static class CreateResourceUnit extends ResourceUnitsTest {

        @Rule
        public JUnitRuleMockery2 context = JUnitRuleMockery2.createFor(JUnitRuleMockery2.Mode.INTERFACES_AND_CLASSES);

        @Mock
        private DomainObjectContainer mockContainer;

        ResourceUnits resourceUnits;

        @Before
        public void setup() {
            resourceUnits = new ResourceUnits();
            resourceUnits.setContainer(mockContainer);
        }


        @Test
        public void createResourceUnit() {
            // given
            final ResourceUnit resourceUnit = new ResourceUnit();

            // expect
            context.checking(new Expectations() {
                {
                    oneOf(mockContainer).newTransientInstance(ResourceUnit.class);
                    will(returnValue(resourceUnit));

                    oneOf(mockContainer).persistIfNotAlready(resourceUnit);
                }
            });

            // when
            final ResourceUnit newResourceUnit = resourceUnits.createResourceUnit("name","1024AB");

            // then
            Assertions.assertThat(newResourceUnit.getName()).isEqualTo("name");
            Assertions.assertThat(newResourceUnit.getLocation()).isEqualTo("1024AB");
            //dependent on postalcode service (internetconnection needed)
            Assertions.assertThat(newResourceUnit.getLatitude()).isEqualTo(52.39674);
            Assertions.assertThat(newResourceUnit.getLongitude()).isEqualTo(4.949224);
        }

    }

}
