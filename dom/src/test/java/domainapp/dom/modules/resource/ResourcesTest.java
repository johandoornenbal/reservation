package domainapp.dom.modules.resource;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import org.apache.isis.applib.query.Query;

import domainapp.dom.FinderInteraction;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by jodo on 21/06/15.
 */
public class ResourcesTest {

    FinderInteraction finderInteraction;

    Resources resources;

    @Before
    public void setup() {
        resources = new Resources() {

            @Override
            protected <T> T firstMatch(Query<T> query) {
                finderInteraction = new FinderInteraction(query, FinderInteraction.FinderMethod.FIRST_MATCH);
                return null;
            }

            @Override
            protected List<Resource> allInstances() {
                finderInteraction = new FinderInteraction(null, FinderInteraction.FinderMethod.ALL_INSTANCES);
                return null;
            }

            @Override
            protected <T> List<T> allMatches(Query<T> query) {
                finderInteraction = new FinderInteraction(query, FinderInteraction.FinderMethod.ALL_MATCHES);
                return null;
            }
        };
    }

    public static class FindByResourceUnit extends ResourcesTest {

        @Test
        public void happyCase() {

            ResourceUnit unit = new ResourceUnit();
            resources.findByResourceUnit(unit);

            assertThat(finderInteraction.getFinderMethod(), is(FinderInteraction.FinderMethod.ALL_MATCHES));
            assertThat(finderInteraction.getQueryName(), is("findByResourceUnit"));
            assertThat(finderInteraction.getArgumentsByParameterName().get("resourceUnit"), is((Object) unit));
            assertThat(finderInteraction.getArgumentsByParameterName().size(), is(1));
        }
    }
}
