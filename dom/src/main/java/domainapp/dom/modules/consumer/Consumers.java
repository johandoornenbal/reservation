package domainapp.dom.modules.consumer;

import java.util.List;

import javax.inject.Inject;

import org.joda.time.LocalDateTime;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.DomainService;
import org.apache.isis.applib.annotation.NatureOfService;
import org.apache.isis.applib.annotation.ParameterLayout;
import org.apache.isis.applib.annotation.Programmatic;
import org.apache.isis.applib.annotation.SemanticsOf;

import org.isisaddons.services.postalcode.Location;
import org.isisaddons.services.postalcode.postcodenunl.Haversine;
import org.isisaddons.services.postalcode.postcodenunl.PostcodeNuService;

import domainapp.dom.ReservationDomainService;
import domainapp.dom.modules.resource.ResourceContributions;
import domainapp.dom.modules.resource.ResourceUnit;
import domainapp.dom.modules.resource.ResourceUnits;

/**
 * Created by jodo on 20/06/15.
 */
@DomainService(repositoryFor = Consumer.class, nature = NatureOfService.VIEW)
public class Consumers extends ReservationDomainService<Consumer> {

    public Consumers() {
        super(Consumers.class, Consumer.class);
    }

    @Action(semantics = SemanticsOf.SAFE)
    public List<Consumer> allConsumers() {
        return allInstances(Consumer.class);
    }

    @Action(semantics = SemanticsOf.SAFE)
    public List<Consumer> findById(String id) {
        return allMatches("findById", "id", id);
    }

    @Action(semantics = SemanticsOf.SAFE)
    @Programmatic
    public List<Consumer> autoComplete(String search) {
        return allMatches("matchId", "search" , search.toLowerCase());
    }


    @Action(semantics = SemanticsOf.NON_IDEMPOTENT)
    public Consumer createConsumer(
            final @ParameterLayout(named = "id") String id
    ) {
        final Consumer newConsumer = newTransientInstance(Consumer.class);
        newConsumer.setId(id);

        persistIfNotAlready(newConsumer);

        return newConsumer;
    }

    @Programmatic
    public ReservationOption findClosestReservationOption(
            final Consumer consumer,
            @ParameterLayout(named = "postalcode")
            final String location,
            @ParameterLayout(named = "start")
            final LocalDateTime start,
            @ParameterLayout(named = "end")
            final LocalDateTime end){

        PostcodeNuService service = new PostcodeNuService();
        Location ConsumerDestinationLocation = service.locationFromPostalCode(null, location);
        //FIND CLOSEST RESOURCEUNIT with free resource
        double bestDistance = 9999999;
        double distance;
        ResourceUnit bestUnit = null;
        for (ResourceUnit unit : resourceUnits.allResourceUnits()){
            distance = Haversine.haversine(
                    unit.getLatitude(),
                    unit.getLongitude(),
                    ConsumerDestinationLocation.getLatitude(),
                    ConsumerDestinationLocation.getLongitude());
            if (ConsumerDestinationLocation.getSucces() && distance < bestDistance)  {
                if (resourceContributions.findResourceForPeriod(unit,start,end)!=null){
                    bestUnit=unit;
                    bestDistance = distance;
                }
            }
        }

        ReservationOption option = new ReservationOption(
                bestUnit,
                consumer,
                start,
                end,
                ConsumerDestinationLocation.getLatitude(),
                ConsumerDestinationLocation.getLongitude(),
                location,
                ConsumerDestinationLocation.getSucces()
                );
        return option;
    }

    @Inject
    private ResourceUnits resourceUnits;

    @Inject
    private ResourceContributions resourceContributions;
}
