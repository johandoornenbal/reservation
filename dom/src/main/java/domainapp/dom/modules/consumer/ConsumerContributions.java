package domainapp.dom.modules.consumer;

import javax.inject.Inject;

import org.joda.time.LocalDateTime;

import org.apache.isis.applib.annotation.DomainService;
import org.apache.isis.applib.annotation.NatureOfService;
import org.apache.isis.applib.annotation.ParameterLayout;

/**
 * Created by jodo on 10/07/15.
 */
@DomainService(repositoryFor = Consumer.class, nature = NatureOfService.VIEW_CONTRIBUTIONS_ONLY)
public class ConsumerContributions {

    public ReservationOption findClosestReservationOption(
            final Consumer consumer,
            @ParameterLayout(named = "postalcode")
            final String location,
            @ParameterLayout(named = "start")
            final LocalDateTime start,
            @ParameterLayout(named = "aantal uren")
            final Integer duration
    ){
        return consumers.findClosestReservationOption(consumer,location,start,start.plusHours(duration));
    }

    @Inject
    private Consumers consumers;

}
