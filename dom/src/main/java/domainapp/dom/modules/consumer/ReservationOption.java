package domainapp.dom.modules.consumer;

import javax.inject.Inject;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import org.apache.isis.applib.annotation.ParameterLayout;
import org.apache.isis.applib.annotation.ViewModel;

import org.isisaddons.services.postalcode.postcodenunl.Haversine;

import domainapp.dom.modules.resource.Resource;
import domainapp.dom.modules.resource.ResourceContributions;
import domainapp.dom.modules.resource.ResourceUnit;

/**
 * Created by jodo on 10/07/15.
 */
@ViewModel
public class ReservationOption {

    public ReservationOption(){}

    public ReservationOption(
            final ResourceUnit unit,
            final Consumer consumer,
            final LocalDateTime start,
            final LocalDateTime end,
            final double latitude,
            final double longitude,
            final String location,
            final boolean succes
    ){
        this.unit = unit;
        this.consumer = consumer;
        this.start = new LocalDateTime(start);
        this.end = end;
        this.startDate = start.toLocalDate();
        this.startHour = start.getHourOfDay();
        this.startMinutes = start.getMinuteOfHour();
        this.endDate = end.toLocalDate();
        this.endHour = end.getHourOfDay();
        this.endMinutes = end.getMinuteOfHour();
        this.latitude = latitude;
        this.longitude = longitude;
        if (latitude>0 && longitude>0 && unit!=null) {
            setDistance(Haversine.haversine(unit.getLatitude(), unit.getLongitude(), latitude, longitude));
        }
        this.location = location;
        this.succes = succes;
    }

    private ResourceUnit unit;
    private Consumer consumer;
    private LocalDateTime start;
    private LocalDateTime end;
    private LocalDate startDate;
    private Integer startHour;
    private Integer startMinutes;
    private LocalDate endDate;
    private Integer endHour;
    private Integer endMinutes;
    private double latitude;
    private double longitude;
    private double distance;
    private String location;
    private boolean succes;

    public boolean getSucces() {
        return succes;
    }

    public void setSucces(final boolean succes) {
        this.succes = succes;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }

    public LocalDateTime getStart() {
        return new LocalDateTime(
                getStartDate().getYear(),
                getStartDate().getMonthOfYear(),
                getStartDate().getDayOfMonth(),
                getStartHour(),
                getStartMinutes()
        );
    }

    public LocalDateTime getEnd() {
        return new LocalDateTime(
                getEndDate().getYear(),
                getEndDate().getMonthOfYear(),
                getEndDate().getDayOfMonth(),
                getEndHour(),
                getEndMinutes()
        );
    }

    public ResourceUnit getUnit() {
        return unit;
    }

    public void setUnit(final ResourceUnit unit) {
        this.unit = unit;
    }

    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(final Consumer consumer) {
        this.consumer = consumer;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(final double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(final double longitude) {
        this.longitude = longitude;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(final double distance) {
        this.distance = distance;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(final LocalDate startDate) {
        this.startDate = startDate;
    }

    public Integer getStartHour() {
        return startHour;
    }

    public void setStartHour(final Integer startHour) {
        this.startHour = startHour;
    }

    public Integer getStartMinutes() {
        return startMinutes;
    }

    public void setStartMinutes(final Integer startMinutes) {
        this.startMinutes = startMinutes;
    }

    public LocalDate getEndDate() {return endDate;}

    public void setEndDate(final LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getEndHour() {
        return endHour;
    }

    public void setEndHour(final Integer endHour) {
        this.endHour = endHour;
    }

    public Integer getEndMinutes() {
        return endMinutes;
    }

    public void setEndMinutes(final Integer endMinutes) {
        this.endMinutes = endMinutes;
    }

    public Resource placeReservation() {
        return resourceContributions.allocateResourceForPeriod(
                getUnit(),
                getStart(),
                getEnd(),
                getConsumer());
    }

    public boolean hidePlaceReservation() {
        if (this.getSucces() && unit!=null) {
            return false;
        }
        return true;
    }

    public ReservationOption retry(final @ParameterLayout(named = "Postalcode") String location) {
        return consumers.findClosestReservationOption(getConsumer(),location,getStart(),getEnd());
    }

    public String default0Retry(final String location) {
        return getLocation();
    }

    public boolean hideRetry() {
        if (this.getSucces()) {
            return true;
        }
        return false;
    }

    public String title(){
        if (unit==null) {
            return "No Place Found";
        }
        return "Reservation in: " +getUnit().getName();
    }

    @Inject
    ResourceContributions resourceContributions;

    @Inject
    Consumers consumers;

}
