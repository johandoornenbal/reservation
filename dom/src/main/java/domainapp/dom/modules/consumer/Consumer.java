package domainapp.dom.modules.consumer;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.VersionStrategy;

import org.apache.isis.applib.annotation.DomainObject;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Title;

import domainapp.dom.ReservationDomainObject;

/**
 * Created by jodo on 20/06/15.
 */
@javax.jdo.annotations.PersistenceCapable(
        identityType= IdentityType.DATASTORE
)
@javax.jdo.annotations.DatastoreIdentity(
        strategy=javax.jdo.annotations.IdGeneratorStrategy.IDENTITY,
        column="id")
@javax.jdo.annotations.Version(
        strategy= VersionStrategy.VERSION_NUMBER,
        column="version")
@javax.jdo.annotations.Queries({
        @javax.jdo.annotations.Query(
                name = "findById", language = "JDOQL",
                value = "SELECT "
                        + "FROM domainapp.dom.modules.consumer.Consumer "
                        + "WHERE id == :id "),
        @javax.jdo.annotations.Query(
                name = "matchId", language = "JDOQL",
                value = "SELECT "
                        + "FROM domainapp.dom.modules.consumer.Consumer "
                        + "WHERE id.toLowerCase().indexOf(:search) >= 0 ")
})
@DomainObject(autoCompleteRepository = Consumers.class, autoCompleteAction = "autoComplete")
public class Consumer extends ReservationDomainObject<Consumer>{

    public Consumer() {
        super("id");
    }

    //region > id (property)
    private String id;

    @MemberOrder(sequence = "1")
    @Column(allowsNull="false")
    @Title()
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }
    //endregion

}
