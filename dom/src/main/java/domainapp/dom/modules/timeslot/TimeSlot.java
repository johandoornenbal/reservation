package domainapp.dom.modules.timeslot;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.VersionStrategy;

import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.ActionLayout;
import org.apache.isis.applib.annotation.BookmarkPolicy;
import org.apache.isis.applib.annotation.Contributed;
import org.apache.isis.applib.annotation.DomainObjectLayout;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Programmatic;
import org.apache.isis.applib.annotation.PropertyLayout;
import org.apache.isis.applib.annotation.SemanticsOf;
import org.apache.isis.applib.annotation.Where;

import domainapp.dom.ReservationDomainObject;
import domainapp.dom.modules.consumer.Consumer;
import domainapp.dom.modules.resource.Resource;
import static org.joda.time.Minutes.minutesBetween;

/**
 * Created by jodo on 20/06/15.
 */
@javax.jdo.annotations.PersistenceCapable(
        identityType= IdentityType.DATASTORE
)
@javax.jdo.annotations.DatastoreIdentity(
        strategy=javax.jdo.annotations.IdGeneratorStrategy.IDENTITY,
        column="id")
@javax.jdo.annotations.Version(
        strategy= VersionStrategy.VERSION_NUMBER,
        column="version")
@javax.jdo.annotations.Queries({
        @javax.jdo.annotations.Query(
                name = "findByResource", language = "JDOQL",
                value = "SELECT "
                        + "FROM domainapp.dom.modules.timeslot.TimeSlot "
                        + "WHERE resource == :resource "),
        @javax.jdo.annotations.Query(
                name = "findByConsumer", language = "JDOQL",
                value = "SELECT "
                        + "FROM domainapp.dom.modules.timeslot.TimeSlot "
                        + "WHERE consumer == :consumer ")
})
//@javax.jdo.annotations.Unique(name="Timeslot_name_UNQ", members = {"name"})
//@DomainObject(autoCompleteAction = "findByName", autoCompleteRepository = TimeSlots.class)
@DomainObjectLayout(
        bookmarking = BookmarkPolicy.AS_ROOT
)
public class TimeSlot extends ReservationDomainObject<TimeSlot> {

    public TimeSlot() {
        super("startDate, endDate, startHour, startMinutes, endHour, endMinutes");
    }

    public String title(){
        return startLDT().toString("yyyy-MM-dd HH:mm") + " - " + endLDT().toString("yyyy-MM-dd HH:mm");
    }

    @Action(semantics = SemanticsOf.SAFE)
    @ActionLayout(contributed = Contributed.AS_ASSOCIATION)
    @MemberOrder(sequence = "1")
    public LocalDateTime getStart(){
        return startLDT();
    }

    @Action(semantics = SemanticsOf.SAFE)
    @ActionLayout(contributed = Contributed.AS_ASSOCIATION)
    @MemberOrder(sequence = "2")
    public LocalDateTime getEnd(){
        return endLDT();
    }

    @Action(semantics = SemanticsOf.SAFE)
    @ActionLayout(contributed = Contributed.AS_ASSOCIATION)
    @MemberOrder(sequence = "3")
    public Integer getLengthInMinutes(){
        return minutesBetween(startLDT(), endLDT()).getMinutes();
    }

    //region > resource (property)
    private Resource resource;

    @MemberOrder(sequence = "4")
    @Column(allowsNull="false")
    public Resource getResource() {
        return resource;
    }

    public void setResource(final Resource resource) {
        this.resource = resource;
    }
    //endregion

    //region > consumer (property)
    private Consumer consumer;

    @MemberOrder(sequence = "4")
    @Column(allowsNull="false")
    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(final Consumer consumer) {
        this.consumer = consumer;
    }
    //endregion

    private LocalDate startDate;

    @Column(allowsNull="false")
    @MemberOrder(sequence = "4")
    @PropertyLayout(hidden = Where.EVERYWHERE)
    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(final LocalDate startDate) {
        this.startDate = startDate;
    }

    //region > startHour (property)
    private Integer startHour;

    @Column(allowsNull="false")
    @MemberOrder(sequence = "4.1")
    @PropertyLayout(hidden = Where.EVERYWHERE)
    public Integer getStartHour() {
        return startHour;
    }

    public void setStartHour(final Integer startHour) {
        this.startHour = startHour;
    }
    //endregion

    //region > startMinutes (property)
    private Integer startMinutes;

    @Column(allowsNull="false")
    @MemberOrder(sequence = "4.2")
    @PropertyLayout(hidden = Where.EVERYWHERE)
    public Integer getStartMinutes() {
        return startMinutes;
    }

    public void setStartMinutes(final Integer startMinutes) {
        this.startMinutes = startMinutes;
    }
    //endregion

    private LocalDate endDate;

    @Column(allowsNull="false")
    @MemberOrder(sequence = "4.3")
    @PropertyLayout(hidden = Where.EVERYWHERE)
    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(final LocalDate endDate) {
        this.endDate = endDate;
    }

    //region > endHour (property)
    private Integer endHour;

    @Column(allowsNull="false")
    @MemberOrder(sequence = "5")
    @PropertyLayout(hidden = Where.EVERYWHERE)
    public Integer getEndHour() {
        return endHour;
    }

    public void setEndHour(final Integer endHour) {
        this.endHour = endHour;
    }
    //endregion

    //region > startMinutes (property)
    private Integer endMinutes;

    @Column(allowsNull="false")
    @MemberOrder(sequence = "6")
    @PropertyLayout(hidden = Where.EVERYWHERE)
    public Integer getEndMinutes() {
        return endMinutes;
    }

    public void setEndMinutes(final Integer endMinutes) {
        this.endMinutes = endMinutes;
    }
    //endregion



    @Programmatic
    private LocalDateTime startLDT() {
        return new LocalDateTime(
                getStartDate().getYear(),
                getStartDate().getMonthOfYear(),
                getStartDate().getDayOfMonth(),
                getStartHour(),
                getStartMinutes());
    }

    @Programmatic
    private LocalDateTime endLDT() {
        return new LocalDateTime(
                getEndDate().getYear(),
                getEndDate().getMonthOfYear(),
                getEndDate().getDayOfMonth(),
                getEndHour(),
                getEndMinutes());
    }

    @Programmatic
    public Period period() {
        return new Period(startLDT(), endLDT());
    }

    @Programmatic
    public Interval interval() {
            return new Interval(startLDT().toDateTime(), endLDT().toDateTime());
    }
}