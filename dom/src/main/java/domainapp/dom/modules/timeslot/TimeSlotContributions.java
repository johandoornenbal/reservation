package domainapp.dom.modules.timeslot;

import java.util.List;

import javax.inject.Inject;

import org.joda.time.LocalDateTime;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.ActionLayout;
import org.apache.isis.applib.annotation.CollectionLayout;
import org.apache.isis.applib.annotation.Contributed;
import org.apache.isis.applib.annotation.DomainService;
import org.apache.isis.applib.annotation.DomainServiceLayout;
import org.apache.isis.applib.annotation.NatureOfService;
import org.apache.isis.applib.annotation.ParameterLayout;
import org.apache.isis.applib.annotation.RenderType;
import org.apache.isis.applib.annotation.SemanticsOf;

import domainapp.dom.modules.consumer.Consumer;
import domainapp.dom.modules.resource.Resource;

/**
 * Created by jodo on 20/06/15.
 */
@DomainService(nature = NatureOfService.VIEW_CONTRIBUTIONS_ONLY)
@DomainServiceLayout()
public class TimeSlotContributions  {

    @Action(semantics = SemanticsOf.IDEMPOTENT)
    public Resource createTimeSlot(
            final @ParameterLayout(named = "start date and time") LocalDateTime startDate,
            final @ParameterLayout(named = "end date and time") LocalDateTime endDate,
            final @ParameterLayout(named = "resource") Resource resource,
            final @ParameterLayout(named = "consumer") Consumer consumer
    ) {
        timeSlots.createTimeSlot(startDate,endDate,resource, consumer);
        return resource;
    }

    @Action(semantics = SemanticsOf.SAFE)
    @ActionLayout(contributed = Contributed.AS_ASSOCIATION)
    @CollectionLayout(render = RenderType.EAGERLY)
    public List<TimeSlot> timeSlots(Resource resource) {
        return timeSlots.findTimeSlotByResource(resource);
    }

    @Action(semantics = SemanticsOf.SAFE)
    @ActionLayout(contributed = Contributed.AS_ASSOCIATION)
    @CollectionLayout(render = RenderType.EAGERLY)
    public List<TimeSlot> timeSlots(Consumer consumer) {
        return timeSlots.findTimeSlotByConsumer(consumer);
    }

    @Inject
    private TimeSlots timeSlots;
}
