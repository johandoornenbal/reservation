package domainapp.dom.modules.timeslot;

import java.util.List;

import org.joda.time.LocalDateTime;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.DomainService;
import org.apache.isis.applib.annotation.NatureOfService;
import org.apache.isis.applib.annotation.ParameterLayout;
import org.apache.isis.applib.annotation.SemanticsOf;

import domainapp.dom.ReservationDomainService;
import domainapp.dom.modules.consumer.Consumer;
import domainapp.dom.modules.resource.Resource;

/**
 * Created by jodo on 20/06/15.
 */
@DomainService(repositoryFor = TimeSlot.class, nature = NatureOfService.DOMAIN)
public class TimeSlots extends ReservationDomainService<TimeSlot> {

    public TimeSlots() {
        super(TimeSlots.class, TimeSlot.class);
    }

    @Action(semantics = SemanticsOf.SAFE)
    public List<TimeSlot> allTimeSlots() {
        return allInstances(TimeSlot.class);
    }

    @Action(semantics = SemanticsOf.SAFE)
    public List<TimeSlot> findTimeSlotByResource(Resource resource) {
        return allMatches("findByResource", "resource", resource);
    }

    @Action(semantics = SemanticsOf.SAFE)
    public List<TimeSlot> findTimeSlotByConsumer(Consumer consumer) {
        return allMatches("findByConsumer", "consumer", consumer);
    }

    @Action(semantics = SemanticsOf.NON_IDEMPOTENT)
    public TimeSlot createTimeSlot(
            final @ParameterLayout(named = "start date and time") LocalDateTime startDate,
            final @ParameterLayout(named = "end date and time") LocalDateTime endDate,
            final @ParameterLayout(named = "resource") Resource resource,
            final @ParameterLayout(named = "consumer") Consumer consumer
    ) {
        final TimeSlot newSlot = newTransientInstance(TimeSlot.class);

        newSlot.setStartDate(startDate.toLocalDate());
        newSlot.setStartHour(startDate.getHourOfDay());
        newSlot.setStartMinutes(startDate.getMinuteOfHour());
        newSlot.setEndDate(endDate.toLocalDate());
        newSlot.setEndHour(endDate.getHourOfDay());
        newSlot.setEndMinutes(endDate.getMinuteOfHour());
        newSlot.setResource(resource);
        newSlot.setConsumer(consumer);

        persistIfNotAlready(newSlot);

        return newSlot;
    }
}
