package domainapp.dom.modules.timeslot;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import static org.joda.time.Days.daysBetween;
import static org.joda.time.Minutes.minutesBetween;

/**
 * Created by jodo on 20/06/15.
 */
public class TestingLocalDateTime {

    public static void main(String[] args) {

        LocalDate start = new LocalDate(2000,01,01);
        LocalDate end = new LocalDate(2000,01,02);

        LocalDateTime start1 = new LocalDateTime(2000,01,01,0,0);
        LocalDateTime end1 = new LocalDateTime(2000,01,02,1,30);

        System.out.println("Testing:");
        System.out.println(daysBetween(start, end).getDays());
        System.out.println(minutesBetween(start, end).getMinutes());
        System.out.println(minutesBetween(start1, end1).getMinutes());

    }

}
