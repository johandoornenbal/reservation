package domainapp.dom.modules.resource;

import java.util.List;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.DomainService;
import org.apache.isis.applib.annotation.NatureOfService;
import org.apache.isis.applib.annotation.ParameterLayout;
import org.apache.isis.applib.annotation.SemanticsOf;

import domainapp.dom.ReservationDomainService;

/**
 * Created by jodo on 20/06/15.
 */
@DomainService(repositoryFor = Resource.class, nature = NatureOfService.DOMAIN)
public class Resources extends ReservationDomainService<Resource> {

    public Resources() {
        super(Resources.class, Resource.class);
    }

    @Action(semantics = SemanticsOf.SAFE)
    public List<Resource> allResources() {
        return allInstances(Resource.class);
    }

    @Action(semantics = SemanticsOf.SAFE)
    public List<Resource> findByResourceUnit(ResourceUnit unit) {
        return allMatches("findByResourceUnit", "resourceUnit" , unit);
    }


    @Action(semantics = SemanticsOf.NON_IDEMPOTENT)
    public Resource createResource(
            final @ParameterLayout(named = "id") Integer id,
            final @ParameterLayout(named = "resource unit") ResourceUnit resourceUnit
    ) {
        final Resource newResource = newTransientInstance(Resource.class);
        newResource.setId(id);
        newResource.setResourceUnit(resourceUnit);

        persistIfNotAlready(newResource);

        return newResource;
    }
}
