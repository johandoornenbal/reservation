package domainapp.dom.modules.resource;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.joda.time.Interval;
import org.joda.time.LocalDateTime;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.ActionLayout;
import org.apache.isis.applib.annotation.CollectionLayout;
import org.apache.isis.applib.annotation.Contributed;
import org.apache.isis.applib.annotation.DomainService;
import org.apache.isis.applib.annotation.NatureOfService;
import org.apache.isis.applib.annotation.ParameterLayout;
import org.apache.isis.applib.annotation.Programmatic;
import org.apache.isis.applib.annotation.RenderType;
import org.apache.isis.applib.annotation.SemanticsOf;

import domainapp.dom.modules.consumer.Consumer;
import domainapp.dom.modules.timeslot.TimeSlot;
import domainapp.dom.modules.timeslot.TimeSlots;
import static org.joda.time.Minutes.minutesBetween;

/**
 * Created by jodo on 20/06/15.
 */
@DomainService(nature = NatureOfService.VIEW_CONTRIBUTIONS_ONLY)
public class ResourceContributions  {

    @Action(semantics = SemanticsOf.NON_IDEMPOTENT)
    public ResourceUnit createResource(
            final Integer id,
            final ResourceUnit resourceUnit
    ) {
        resources.createResource(id,resourceUnit);
        return resourceUnit;
    }

    @Action(semantics = SemanticsOf.SAFE)
    @ActionLayout(contributed = Contributed.AS_ASSOCIATION)
    @CollectionLayout(render = RenderType.EAGERLY)
    public List<Resource> resources(ResourceUnit resourceUnit) {
        return resources.findByResourceUnit(resourceUnit);
    }

    @Action(semantics = SemanticsOf.SAFE)
    @ActionLayout(contributed = Contributed.AS_ACTION)
    public Resource findResourceForPeriod(final ResourceUnit unit, final LocalDateTime start, final LocalDateTime end) {
        Interval interval = new Interval(start.toDateTime(), end.toDateTime());
        for (Resource resource : resources.findByResourceUnit(unit)) {
            boolean overlapNotFound = true;
            for (TimeSlot timeSlot: timeSlots.findTimeSlotByResource(resource)) {
                System.out.println("Testing:");
                System.out.println("timeSlot interval: " + timeSlot.interval());
                System.out.println("interval from start to end: " + interval);
                if (timeSlot.interval().overlaps(interval) && overlapNotFound) {
                    overlapNotFound = false;
                }
            }
            if (overlapNotFound) {
                return resource;
            }
        }
        return null;
    }

    @Action(semantics = SemanticsOf.SAFE)
    @ActionLayout(contributed = Contributed.AS_ACTION)
    public Resource allocateResourceForPeriod(
            final @ParameterLayout(named = "unit") ResourceUnit unit,
            final @ParameterLayout(named = "start") LocalDateTime start,
            final @ParameterLayout(named = "end") LocalDateTime end,
            final @ParameterLayout(named = "consumer") Consumer consumer) {
        try {
            return allocateResourceForIntervalV2(unit, start, end, consumer).getResource();
        } catch (Exception e) {
            return null;
        }
    }

    @Programmatic
    private TimeSlot allocateResourceForIntervalV2(
            final ResourceUnit unit,
            final LocalDateTime start,
            final LocalDateTime end,
            final Consumer consumer) {
        Interval interval = new Interval(start.toDateTime(), end.toDateTime());
        System.out.println("interval: " + interval);
        List<Resource> candidates = new ArrayList<Resource>();
        for (Resource resource : resources.findByResourceUnit(unit)) {
            System.out.println("Unit: " + unit.getName());
            System.out.println("Resource: " + resource.title());
            boolean overlapNotFound = true;
            for (TimeSlot timeSlot: timeSlots.findTimeSlotByResource(resource)) {
                System.out.println("timeSlot interval: " + timeSlot.interval());
                System.out.println("interval from start to end: " + interval);
                System.out.println("overlapNotFound: " + overlapNotFound);
                if (timeSlot.interval().overlaps(interval) && overlapNotFound) {
                    overlapNotFound = false;
                }
            }
            if (overlapNotFound) {
                candidates.add(resource);
            }
        }
        //pick resource with minimum interval to next reservation of resource if there are any
        if (candidates.size()>0) {
            Resource bestCandidate = candidates.get(0);
            Integer bestTime = 123456789;
            System.out.println("bestCandidate: " + bestCandidate);
            System.out.println("bestTime: " + bestTime);
            for (Resource candidate : candidates) {
                //call timeToNextSlot
                if (timeToNextSlot(interval, candidate) < bestTime) {
                    bestTime = timeToNextSlot(interval, candidate);
                    bestCandidate = candidate;
                    System.out.println("bestCandidate: " + bestCandidate);
                    System.out.println("bestTime: " + bestTime);
                }
            }
            return timeSlots.createTimeSlot(start, end, bestCandidate, consumer);
        }
        return null;
    }

    @Programmatic
    private Integer timeToNextSlot(final Interval interval, final Resource resource){
        Integer timeInMinutes = 123456789;
        TimeSlot foundSlot = new TimeSlot();
        for (TimeSlot slot : timeSlots.findTimeSlotByResource(resource)) {
            //only those slots after interval
            if (slot.interval().isAfter(interval)) {
                if (minutesBetween(interval.getEnd(),slot.interval().getStart()).getMinutes() < 123456789) {
                    timeInMinutes = minutesBetween(interval.getEnd(), slot.interval().getStart()).getMinutes();
                    foundSlot = slot;
                }
            }
        }
        // no next slot or far far away
        return timeInMinutes;
    }

    @Inject
    private Resources resources;

    @Inject
    private TimeSlots timeSlots;
}
