package domainapp.dom.modules.resource;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.VersionStrategy;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.DomainObject;
import org.apache.isis.applib.annotation.MemberOrder;
import org.apache.isis.applib.annotation.Parameter;
import org.apache.isis.applib.annotation.ParameterLayout;
import org.apache.isis.applib.annotation.SemanticsOf;
import org.apache.isis.applib.annotation.Title;

import org.isisaddons.services.postalcode.Location;
import org.isisaddons.services.postalcode.postcodenunl.PostcodeNuService;

import domainapp.dom.ReservationDomainObject;

/**
 * Created by jodo on 20/06/15.
 */
@javax.jdo.annotations.PersistenceCapable(
        identityType= IdentityType.DATASTORE
)
@javax.jdo.annotations.DatastoreIdentity(
        strategy=javax.jdo.annotations.IdGeneratorStrategy.IDENTITY,
        column="id")
@javax.jdo.annotations.Version(
        strategy= VersionStrategy.VERSION_NUMBER,
        column="version")
@DomainObject(autoCompleteRepository =  ResourceUnits.class, autoCompleteAction = "autoComplete")
public class ResourceUnit extends ReservationDomainObject<ResourceUnit> {
    public ResourceUnit() {
        super("name, location");
    }

    //region > name (property)
    private String name;

    @Title
    @MemberOrder(sequence = "1")
    @Column(allowsNull="false")
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
    //endregion

    //region > location (property)
    private String location;

    @MemberOrder(sequence = "2")
    @Column(allowsNull="false")
    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }
    //endregion

    //latitude /////////////////////////////////////////////////////////////////////////////////////

    private double latitude;

    @javax.jdo.annotations.Column(allowsNull = "true")
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    //longitude /////////////////////////////////////////////////////////////////////////////////////

    private double longitude;

    @javax.jdo.annotations.Column(allowsNull = "true")
    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Action(semantics= SemanticsOf.IDEMPOTENT)
    public ResourceUnit updateLocation(
            @ParameterLayout(
                    named = "postcode"
            )
            @Parameter(regexPattern="^[1-9]{1}[0-9]{3}[A-Z]{2}$")
            String location
    ){
        PostcodeNuService service = new PostcodeNuService();
        Location UnitLocation = service.locationFromPostalCode(null, location);
        this.setLongitude(UnitLocation.getLongitude());
        this.setLatitude(UnitLocation.getLatitude());
        this.setLocation(location);
        return this;
    }

    public String default0UpdateLocation() {
        return getLocation();
    }

}
