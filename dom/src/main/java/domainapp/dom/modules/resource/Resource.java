package domainapp.dom.modules.resource;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.VersionStrategy;

import org.apache.isis.applib.annotation.DomainObject;
import org.apache.isis.applib.annotation.MemberOrder;

import domainapp.dom.ReservationDomainObject;

/**
 * Created by jodo on 20/06/15.
 */
@javax.jdo.annotations.PersistenceCapable(
        identityType= IdentityType.DATASTORE
)
@javax.jdo.annotations.DatastoreIdentity(
        strategy=javax.jdo.annotations.IdGeneratorStrategy.IDENTITY,
        column="id")
@javax.jdo.annotations.Version(
        strategy= VersionStrategy.VERSION_NUMBER,
        column="version")
@javax.jdo.annotations.Queries({
        @javax.jdo.annotations.Query(
                name = "findByResourceUnit", language = "JDOQL",
                value = "SELECT "
                        + "FROM domainapp.dom.modules.resource.Resource "
                        + "WHERE resourceUnit == :resourceUnit ")
})
@DomainObject()
public class Resource extends ReservationDomainObject<Resource>{

    public Resource() {
        super("id, resourceUnit");
    }

    public String title() {
        return "resource " + getId().toString() + " of " + getResourceUnit().getName();
    }

    //region > id (property)
    private Integer id;

    @MemberOrder(sequence = "1")
    @Column(allowsNull="false")
    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }
    //endregion

    //region > resourceUnit (property)
    private ResourceUnit resourceUnit;

    @MemberOrder(sequence = "2")
    @Column(allowsNull="false")
    public ResourceUnit getResourceUnit() {
        return resourceUnit;
    }

    public void setResourceUnit(final ResourceUnit resourceUnit) {
        this.resourceUnit = resourceUnit;
    }
    //endregion

}
