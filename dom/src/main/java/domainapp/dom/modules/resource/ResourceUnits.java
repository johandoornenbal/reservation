package domainapp.dom.modules.resource;

import java.util.List;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.DomainService;
import org.apache.isis.applib.annotation.NatureOfService;
import org.apache.isis.applib.annotation.Parameter;
import org.apache.isis.applib.annotation.ParameterLayout;
import org.apache.isis.applib.annotation.Programmatic;
import org.apache.isis.applib.annotation.SemanticsOf;

import org.isisaddons.services.postalcode.Location;
import org.isisaddons.services.postalcode.postcodenunl.PostcodeNuService;

import domainapp.dom.ReservationDomainService;

/**
 * Created by jodo on 20/06/15.
 */
@DomainService(repositoryFor = ResourceUnit.class, nature = NatureOfService.VIEW_MENU_ONLY)
public class ResourceUnits extends ReservationDomainService<ResourceUnit> {

    public ResourceUnits() {
        super(ResourceUnits.class, ResourceUnit.class);
    }

    @Action(semantics = SemanticsOf.SAFE)
    public List<ResourceUnit> allResourceUnits() {
        return allInstances(ResourceUnit.class);
    }

    @Action(semantics = SemanticsOf.SAFE)
    @Programmatic
    public List<ResourceUnit> autoComplete(String search) {
        return allResourceUnits();
    }

    @Action(semantics = SemanticsOf.NON_IDEMPOTENT)
    public ResourceUnit createResourceUnit(
            final @ParameterLayout(named = "name") String name,
            final
            @ParameterLayout(named = "postalcode")
            @Parameter(regexPattern="^[1-9]{1}[0-9]{3}[A-Z]{2}$")
            String location
    ) {
        final ResourceUnit newResourceUnit = newTransientInstance(ResourceUnit.class);
        PostcodeNuService service = new PostcodeNuService();
        Location unitLocation = service.locationFromPostalCode(null, location);
        newResourceUnit.setName(name);
        newResourceUnit.setLocation(location);
        newResourceUnit.setLongitude(unitLocation.getLongitude());
        newResourceUnit.setLatitude(unitLocation.getLatitude());

        persistIfNotAlready(newResourceUnit);

        return newResourceUnit;
    }
}
