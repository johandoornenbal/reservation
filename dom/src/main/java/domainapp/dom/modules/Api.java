package domainapp.dom.modules;

import java.util.List;

import javax.inject.Inject;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import org.apache.isis.applib.annotation.DomainService;
import org.apache.isis.applib.annotation.NatureOfService;

import domainapp.dom.modules.consumer.Consumer;
import domainapp.dom.modules.consumer.Consumers;
import domainapp.dom.modules.consumer.ReservationOption;

/**
 * Created by jodo on 17/07/15.
 */
@DomainService(nature = NatureOfService.VIEW_MENU_ONLY)
public class Api {

    public List<Consumer> collectConsumers() {
        return consumers.allConsumers();
    }

    public ReservationOption findOptionToday(
            final String consumerString,
            final String location,
            final Integer hour,
            final Integer minutes,
            final Integer durationMinutes){

        Consumer consumer = consumers.findById(consumerString).get(0);
        LocalDate now = LocalDate.now();
        LocalDateTime dateTimeStart = new LocalDateTime(now.getYear(), now.getMonthOfYear(), now.getDayOfMonth(), hour, minutes);
        LocalDateTime dateTimeEnd = dateTimeStart.plusMinutes(durationMinutes);
        return consumers.findClosestReservationOption(consumer, location, dateTimeStart, dateTimeEnd);
    }

    @Inject
    private Consumers consumers;

}
