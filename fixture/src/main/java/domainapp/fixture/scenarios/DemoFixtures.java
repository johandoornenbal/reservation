package domainapp.fixture.scenarios;

import org.apache.isis.applib.fixturescripts.FixtureScript;

import domainapp.fixture.modules.reservation.CreateConsumers;
import domainapp.fixture.modules.reservation.CreateResourceUnits;
import domainapp.fixture.modules.reservation.CreateResources;
import domainapp.fixture.modules.reservation.CreateTimeSlots;

/**
 * Created by jodo on 29/06/15.
 */
public class DemoFixtures extends FixtureScript {

    @Override protected void execute(final ExecutionContext executionContext) {
        executionContext.executeChild(this, new CreateConsumers());
        executionContext.executeChild(this, new CreateResourceUnits());
        executionContext.executeChild(this, new CreateResources());
        executionContext.executeChild(this, new CreateTimeSlots());
    }
}
