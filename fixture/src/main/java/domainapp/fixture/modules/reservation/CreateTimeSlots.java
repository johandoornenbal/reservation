package domainapp.fixture.modules.reservation;

import javax.inject.Inject;

import org.joda.time.LocalDateTime;

import domainapp.dom.modules.consumer.Consumers;
import domainapp.dom.modules.resource.ResourceUnits;
import domainapp.dom.modules.resource.Resources;

/**
 * Created by jodo on 29/06/15.
 */
public class CreateTimeSlots extends TimeSlotAbstract{

    @Override protected void execute(final ExecutionContext executionContext) {


        // all resource booked in period
        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 10, 0),
                new LocalDateTime(2015, 01, 01, 10, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(0)).get(0),
                consumers.findById("AA-BB-12").get(0),
                executionContext
        );

        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 10, 0),
                new LocalDateTime(2015, 01, 01, 10, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(0)).get(1),
                consumers.findById("AA-BB-13").get(0),
                executionContext
        );

        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 10, 0),
                new LocalDateTime(2015, 01, 01, 10, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(0)).get(2),
                consumers.findById("AA-BB-14").get(0),
                executionContext
        );

        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 10, 0),
                new LocalDateTime(2015, 01, 01, 10, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(0)).get(3),
                consumers.findById("AA-BB-15").get(0),
                executionContext
        );

        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 10, 0),
                new LocalDateTime(2015, 01, 01, 10, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(0)).get(4),
                consumers.findById("AA-BB-16").get(0),
                executionContext
        );
        //end all resource booked in period


        //4 of 5 resource booked in period
        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 11, 0),
                new LocalDateTime(2015, 01, 01, 12, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(0)).get(0),
                consumers.findById("AA-BB-17").get(0),
                executionContext
        );

        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 11, 0),
                new LocalDateTime(2015, 01, 01, 12, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(0)).get(1),
                consumers.findById("AA-BB-18").get(0),
                executionContext
        );

        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 11, 0),
                new LocalDateTime(2015, 01, 01, 12, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(0)).get(2),
                consumers.findById("AA-BB-19").get(0),
                executionContext
        );

        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 11, 0),
                new LocalDateTime(2015, 01, 01, 12, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(0)).get(3),
                consumers.findById("AA-BB-20").get(0),
                executionContext
        );
        //end 4 of 5 resource booked in period

        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 13, 0),
                new LocalDateTime(2015, 01, 01, 14, 0),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(0)).get(1),
                consumers.findById("AA-BB-11").get(0),
                executionContext
        );

        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 10, 0),
                new LocalDateTime(2015, 01, 01, 10, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(1)).get(0),
                consumers.findById("AA-BB-21").get(0),
                executionContext
        );

        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 10, 0),
                new LocalDateTime(2015, 01, 01, 10, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(1)).get(1),
                consumers.findById("AA-BB-22").get(0),
                executionContext
        );

        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 10, 0),
                new LocalDateTime(2015, 01, 01, 10, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(2)).get(0),
                consumers.findById("AA-BB-23").get(0),
                executionContext
        );

        createTimeSlot(
                new LocalDateTime(2015, 01, 01, 10, 0),
                new LocalDateTime(2015, 01, 01, 10, 30),
                resources.findByResourceUnit(resourceUnits.allResourceUnits().get(2)).get(1),
                consumers.findById("AA-BB-24").get(0),
                executionContext
        );
    }

    @Inject
    ResourceUnits resourceUnits;

    @Inject
    Resources resources;

    @Inject
    Consumers consumers;
}
