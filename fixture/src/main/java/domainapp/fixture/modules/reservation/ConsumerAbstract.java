package domainapp.fixture.modules.reservation;

import javax.inject.Inject;

import org.apache.isis.applib.fixturescripts.FixtureScript;

import domainapp.dom.modules.consumer.Consumer;
import domainapp.dom.modules.consumer.Consumers;

/**
 * Created by jodo on 29/06/15.
 */
public abstract class ConsumerAbstract extends FixtureScript{

    @Override
    protected abstract void execute(ExecutionContext executionContext);

    protected Consumer createConsumer(String id, ExecutionContext executionContext) {
        Consumer newConsumer = consumers.createConsumer(id);
        return executionContext.addResult(this, newConsumer);
    }


    @Inject
    private Consumers consumers;

}
