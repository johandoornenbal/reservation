package domainapp.fixture.modules.reservation;

import javax.inject.Inject;

import org.apache.isis.applib.fixturescripts.FixtureScript;

import domainapp.dom.modules.resource.ResourceUnit;
import domainapp.dom.modules.resource.ResourceUnits;

/**
 * Created by jodo on 29/06/15.
 */
public abstract class ResourceUnitAbstract extends FixtureScript{

    @Override
    protected abstract void execute(ExecutionContext executionContext);

    protected ResourceUnit createResourceUnit(String name, String location, ExecutionContext executionContext) {
        ResourceUnit newUnit = resourceUnits.createResourceUnit(name, location);
        return executionContext.addResult(this, newUnit);
    }


    @Inject
    private ResourceUnits resourceUnits;

}
