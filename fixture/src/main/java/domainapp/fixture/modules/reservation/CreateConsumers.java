package domainapp.fixture.modules.reservation;

/**
 * Created by jodo on 29/06/15.
 */
public class CreateConsumers extends ConsumerAbstract {

    @Override protected void execute(final ExecutionContext executionContext) {

        createConsumer("AA-BB-11", executionContext);
        createConsumer("AA-BB-12", executionContext);
        createConsumer("AA-BB-13", executionContext);
        createConsumer("AA-BB-14", executionContext);
        createConsumer("AA-BB-15", executionContext);
        createConsumer("AA-BB-16", executionContext);
        createConsumer("AA-BB-17", executionContext);
        createConsumer("AA-BB-18", executionContext);
        createConsumer("AA-BB-19", executionContext);
        createConsumer("AA-BB-20", executionContext);
        createConsumer("AA-BB-21", executionContext);
        createConsumer("AA-BB-22", executionContext);
        createConsumer("AA-BB-23", executionContext);
        createConsumer("AA-BB-24", executionContext);
        createConsumer("01-02-BB", executionContext);
        createConsumer("BC-DE-23", executionContext);
    }


}
