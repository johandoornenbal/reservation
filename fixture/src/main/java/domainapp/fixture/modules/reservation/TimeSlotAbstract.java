package domainapp.fixture.modules.reservation;

import javax.inject.Inject;

import org.joda.time.LocalDateTime;

import org.apache.isis.applib.fixturescripts.FixtureScript;

import domainapp.dom.modules.consumer.Consumer;
import domainapp.dom.modules.resource.Resource;
import domainapp.dom.modules.timeslot.TimeSlot;
import domainapp.dom.modules.timeslot.TimeSlots;

/**
 * Created by jodo on 29/06/15.
 */
public abstract class TimeSlotAbstract extends FixtureScript{

    @Override
    protected abstract void execute(ExecutionContext executionContext);

    protected TimeSlot createTimeSlot(
            LocalDateTime start,
            LocalDateTime end,
            Resource resource,
            Consumer consumer,
            ExecutionContext executionContext) {
        TimeSlot newTimeSlot = timeSlots.createTimeSlot(start, end, resource, consumer);
        return executionContext.addResult(this, newTimeSlot);
    }


    @Inject
    private TimeSlots timeSlots;

}
