package domainapp.fixture.modules.reservation;

import javax.inject.Inject;

import org.apache.isis.applib.fixturescripts.FixtureScript;

import domainapp.dom.modules.resource.Resource;
import domainapp.dom.modules.resource.ResourceUnit;
import domainapp.dom.modules.resource.Resources;

/**
 * Created by jodo on 29/06/15.
 */
public abstract class ResourceAbstract extends FixtureScript{

    @Override
    protected abstract void execute(ExecutionContext executionContext);

    protected Resource createResource(Integer number, ResourceUnit unit, ExecutionContext executionContext) {
        Resource newResource = resources.createResource(number, unit);
        return executionContext.addResult(this, newResource);
    }


    @Inject
    private Resources resources;

}
