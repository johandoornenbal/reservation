package domainapp.fixture.modules.reservation;

/**
 * Created by jodo on 29/06/15.
 */
public class CreateResourceUnits extends ResourceUnitAbstract {

    @Override protected void execute(final ExecutionContext executionContext) {
        createResourceUnit("Unit1", "8926PJ", executionContext);
        createResourceUnit("Unit2", "9084DD", executionContext);
        createResourceUnit("Unit3", "8914AB", executionContext);
    }
}
