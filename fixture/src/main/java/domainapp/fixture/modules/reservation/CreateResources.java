package domainapp.fixture.modules.reservation;

import javax.inject.Inject;

import domainapp.dom.modules.resource.ResourceUnits;

/**
 * Created by jodo on 29/06/15.
 */
public class CreateResources extends ResourceAbstract {

    @Override protected void execute(final ExecutionContext executionContext) {

        createResource(1,resourceUnits.allResourceUnits().get(0), executionContext);
        createResource(2,resourceUnits.allResourceUnits().get(0),executionContext);
        createResource(3,resourceUnits.allResourceUnits().get(0),executionContext);
        createResource(4,resourceUnits.allResourceUnits().get(0),executionContext);
        createResource(5,resourceUnits.allResourceUnits().get(0), executionContext);

        createResource(1,resourceUnits.allResourceUnits().get(1), executionContext);
        createResource(2,resourceUnits.allResourceUnits().get(1),executionContext);

        createResource(1,resourceUnits.allResourceUnits().get(2), executionContext);
        createResource(2,resourceUnits.allResourceUnits().get(2),executionContext);
    }

    @Inject
    ResourceUnits resourceUnits;

}
